//
//  ViewController.h
//  SwipeMath
//
//  Created by Skip Barker on 10/12/13.
//  Copyright (c) 2013 Mutt Studios. All rights reserved.
//
//  This file is part of SwipeMath.
//
//  SwipeMath is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  SwipeMath is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with SwipeMath.  If not, see <http://www.gnu.org/licenses/>.

#import <UIKit/UIKit.h>
#import <AudioToolbox/AudioToolbox.h>

@interface ViewController : UIViewController {
    UILabel *number1;
    UILabel *welcomeLabel;
    IBOutlet UIButton *tapForAnswer;
    IBOutlet UIBarButtonItem *homeButton;
    IBOutlet UIButton *startButton;
    SystemSoundID soundClick;
    SystemSoundID soundSwitch;
    
}

- (IBAction)swipeLeft:(UISwipeGestureRecognizer *)sender;
- (IBAction)swipeRight:(UISwipeGestureRecognizer *)sender;
- (IBAction)swipeDown:(UISwipeGestureRecognizer *)sender;
- (IBAction)swipeUp:(UISwipeGestureRecognizer *)sender;
- (IBAction)tapDetected:(UITapGestureRecognizer *)sender;
- (IBAction)tapForAnswer:(id)sender;
- (IBAction)startButton:(id)sender;




@property (nonatomic,assign) int intNum1;
@property (nonatomic,assign) int intNum2;
@property (nonatomic,assign) int startNum;
@property (nonatomic,assign) int endNum;
@property (nonatomic,assign) int fromNumber;
@property (nonatomic,assign) int toNumber;
@property (nonatomic,assign) int modifier;
@property (nonatomic,assign) int intAnswer;
@property (nonatomic,assign) int intTapForAnswer;
@property (nonatomic,assign) float floatAnswer;
@property (nonatomic,retain) IBOutlet UILabel *number1;
@property (nonatomic,retain) IBOutlet UILabel *welcomeLabel;


@end
