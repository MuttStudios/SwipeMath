//
//  ViewController.m
//  SwipeMath
//
//  Created by Skip Barker on 10/12/13.
//  Copyright (c) 2013 Mutt Studios. All rights reserved.
//
//  This file is part of SwipeMath.
//
//  SwipeMath is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  SwipeMath is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with SwipeMath.  If not, see <http://www.gnu.org/licenses/>.

#import "ViewController.h"

@interface ViewController ()

@end


@implementation ViewController

@synthesize number1,intNum1,intNum2,intAnswer,floatAnswer,modifier,welcomeLabel,fromNumber,toNumber,endNum,startNum,intTapForAnswer;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
//    intStartingNumber = 1;
//    intEndingNumber = 24;

    self.view.backgroundColor = [[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"tile-squares.png"]];
    
    //Set bit to keep track of when double tap on screen displays an answer.
    //0 = no, 1=yes
    intTapForAnswer = 0;
    
    //Call welcome
    [self performSelector:@selector(welcome:)withObject:nil];
    
    //find URL to audio file
    NSURL *clickSound   = [[NSBundle mainBundle] URLForResource: @"click" withExtension: @"mp3"];
    NSURL *switchSound   = [[NSBundle mainBundle] URLForResource: @"switch" withExtension: @"mp3"];
    //initialize SystemSounID variable with file URL
    AudioServicesCreateSystemSoundID (CFBridgingRetain(clickSound), &soundClick);
    AudioServicesCreateSystemSoundID (CFBridgingRetain(switchSound), &soundSwitch);
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//Beginning Swipe Actions
- (IBAction)swipeLeft:(UIGestureRecognizer *)sender {
    

    modifier = 0;
    AudioServicesPlaySystemSound(soundSwitch);
    [self performSelector:@selector(question:) withObject:nil];
    
}

- (IBAction)swipeRight:(UIGestureRecognizer *)sender {

    modifier = 1;
    AudioServicesPlaySystemSound(soundSwitch);
    
    [self performSelector:@selector(question:) withObject:nil];
    
}

- (IBAction)swipeUp:(UIGestureRecognizer *)sender {

    modifier = 2;
    AudioServicesPlaySystemSound(soundSwitch);
    
    [self performSelector:@selector(question:) withObject:nil];
    
    
}

- (IBAction)swipeDown:(UISwipeGestureRecognizer *)sender {

    modifier = 3;
    AudioServicesPlaySystemSound(soundSwitch);
    [self performSelector:@selector(question:) withObject:nil];
    
}

//Beginning Tap Actions

//tapDetected takes you back to the welcome/settings screen.
- (IBAction)tapDetected:(UITapGestureRecognizer *)sender {
    [self performSelector:@selector(welcome:) withObject:nil];
}

//Beginning buttons

//This is for the button to show answers.
- (IBAction)tapForAnswer:(id)sender {
    AudioServicesPlaySystemSound(soundClick);
    
    if (intTapForAnswer == 0) {
        modifier = (arc4random()%4);
        [self performSelector:@selector(question:) withObject:nil];
    } else {
        [self performSelector:@selector(answer:) withObject:nil];
    }

}

//Home or Settings button
- (IBAction)homeButton:(id)sender {
    AudioServicesPlaySystemSound(soundClick);
    [self performSelector:@selector(welcome:) withObject:nil];
}

//Start Button
- (IBAction)startButton:(id)sender {
    modifier = 0;
    AudioServicesPlaySystemSound(soundClick);
    [self performSelector:@selector(question:) withObject:nil];
}

//Now we start working on the code for generating random questions.

- (void)question:(id)sender {
    //Hide the number range settings
    
    //Set answer bit.
    intTapForAnswer = 1;

    startButton.hidden = TRUE;
    welcomeLabel.text = @"";
    
    //Show the tapForAnswer button
    tapForAnswer.hidden = FALSE;
   
    
    //Set the range using the input from the welcome / settings screen.
//    fromNumber = [startingNumberTextField.text intValue];
//    toNumber = [endingNumberTextField.text intValue];
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"properties.plist"];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath: path]) //4
    {
        //NSString *bundle = [[NSBundle mainBundle] pathForResource:@”data” ofType:@”plist”]; //5
        NSString *bundle = [[NSBundle mainBundle] pathForResource:@"properties" ofType:@"plist"];
        [fileManager copyItemAtPath:bundle toPath: path error:&error]; //6
    }
    
    NSMutableDictionary *myDict = [NSMutableDictionary dictionaryWithContentsOfFile:path];
    
    fromNumber = [[myDict objectForKey:@"start"] integerValue];
    toNumber = [[myDict objectForKey:@"end"] integerValue] +1;
    
    
    //Create and show an addition problem.
    if (modifier == 0) {
        //Then we need to get our random numbers.
        intNum1 = (arc4random()%(toNumber-fromNumber))+fromNumber;
        intNum2 = (arc4random()%(toNumber-fromNumber))+fromNumber;
        
        intAnswer = intNum1 + intNum2;
        //Show the problem
        number1.text = [NSString stringWithFormat:@"%d + %d", intNum1,intNum2];
        NSLog(@"%i",intAnswer);
    }
    //Create and show a subtraction problem.
    if (modifier == 1) {
        //Then we need to get our random numbers.
        intNum1 = (arc4random()%(toNumber-fromNumber))+fromNumber;
        intNum2 = (arc4random()%(toNumber-fromNumber))+fromNumber;
        
        intAnswer = intNum1 - intNum2;
        //Show the problem
        number1.text = [NSString stringWithFormat:@"%d - %d", intNum1,intNum2];
        NSLog(@"%i",intAnswer);
    }
    //Create and show a multiplication problem.
    if (modifier == 2) {
        //Then we need to get our random numbers.
        intNum1 = (arc4random()%(toNumber-fromNumber))+fromNumber;
        intNum2 = (arc4random()%(toNumber-fromNumber))+fromNumber;
        
        intAnswer = intNum1 * intNum2;
        //Show the problem
        number1.text = [NSString stringWithFormat:@"%d x %d", intNum1,intNum2];
        NSLog(@"%i",intAnswer);
    }
    //Create and show a division problem.
    if (modifier == 3) {
        //Then we need to get our random numbers.
        intNum1 = (arc4random()%(toNumber-fromNumber))+fromNumber;
        intNum2 = (arc4random()%(toNumber-fromNumber))+fromNumber;
        
        //We will do this to make sure num1 is always bigger than num2.
        //This is to make the division problems a little easier.
        while (intNum2 > intNum1) {
            intNum1 = (arc4random()%(toNumber-fromNumber))+fromNumber;
            intNum2 = (arc4random()%(toNumber-fromNumber))+fromNumber;
        }
        
        floatAnswer = (intNum1 + .00) / (intNum2 + .00);
        //Show the problem
        number1.text = [NSString stringWithFormat:@"%d ÷ %d", intNum1,intNum2];
    }
    

}

- (void)answer:(id)sender {
    welcomeLabel.text = @"";
    tapForAnswer.hidden = TRUE;
    intTapForAnswer = 0;
    if (modifier == 3) {
        NSLog(@"%.2f",floatAnswer);
        number1.text = [NSString stringWithFormat:@"%.2f", floatAnswer];
    } else {
        NSLog(@"%i",intAnswer);
        //swipeForAnswerLabel.text = @"";
        number1.text = [NSString stringWithFormat:@"%i", intAnswer];
    }
    
}

//Welcome or settings display
- (void)welcome:(id)sender {
    startButton.hidden = FALSE;
    
    //Hide Show Answer Button
    tapForAnswer.hidden = TRUE;
    
    //Hide question labels.
    number1.text = @"";
    
    //Show welcome text.
    welcomeLabel.text = @"The direction of your swipe determines your math problem.\r\rGo to Settings to set your number range\r\rleft for addition\rright for subtraction\rup for multiplication\rdown for division";
    //welcomeLabel.text = @"";
}

//Dismiss keyboard for number range.
//- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
//{
//    [startingNumberTextField resignFirstResponder];
//    [endingNumberTextField resignFirstResponder];
//}

//- (IBAction)valueChanged:(UIStepper *)sender {
//    
//    double value = [sender value];
//    intStartingNumber = [sender value];
//    AudioServicesPlaySystemSound(soundClick);
//    [number1setting setText:[NSString stringWithFormat:@"%d", (int)value]];
//}
//
//- (IBAction)valueChanged2:(UIStepper *)sender {
//    
//    double value = [sender value];
//    intEndingNumber = [sender value];
//    AudioServicesPlaySystemSound(soundClick);
//    [number2setting setText:[NSString stringWithFormat:@"%d", (int)value]];
//}

@end
