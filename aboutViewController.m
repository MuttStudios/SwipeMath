//
//  aboutViewController.m
//  SwipeMath
//
//  Created by Skip Barker on 10/20/13.
//  Copyright (c) 2013 Mutt Studios. All rights reserved.
//
//  This file is part of SwipeMath.
//
//  SwipeMath is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  SwipeMath is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with SwipeMath.  If not, see <http://www.gnu.org/licenses/>.

#import "aboutViewController.h"

@interface aboutViewController ()

@end

@implementation aboutViewController
@synthesize valueChanged,valueChanged2,startNum,endNum,number1setting,number2setting;
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"properties.plist"];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath: path]) //4
    {
        //NSString *bundle = [[NSBundle mainBundle] pathForResource:@”data” ofType:@”plist”]; //5
        NSString *bundle = [[NSBundle mainBundle] pathForResource:@"properties" ofType:@"plist"];
        [fileManager copyItemAtPath:bundle toPath: path error:&error]; //6
    }
    
    NSMutableDictionary *myDict = [NSMutableDictionary dictionaryWithContentsOfFile:path];
    
    startNum = [[myDict objectForKey:@"start"] integerValue];
    endNum = [[myDict objectForKey:@"end"] integerValue];
    
    valueChanged.value = startNum;
    valueChanged2.value = endNum;
    
    NSLog(@"%d",startNum);
    NSLog(@"%d",endNum);
    
    
    number1setting.text = [NSString stringWithFormat:@"%d", startNum];
    number2setting.text = [NSString stringWithFormat:@"%d", endNum];

    
    //find URL to audio file
    NSURL *clickSound   = [[NSBundle mainBundle] URLForResource: @"click" withExtension: @"mp3"];
    NSURL *switchSound   = [[NSBundle mainBundle] URLForResource: @"switch" withExtension: @"mp3"];
    //initialize SystemSounID variable with file URL
    AudioServicesCreateSystemSoundID (CFBridgingRetain(clickSound), &soundClick);
    AudioServicesCreateSystemSoundID (CFBridgingRetain(switchSound), &soundSwitch);
    
    

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
//{
//#warning Potentially incomplete method implementation.
//    // Return the number of sections.
//    return 0;
//}
//
//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
//{
//#warning Incomplete method implementation.
//    // Return the number of rows in the section.
//    return 0;
//}
//
//- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    static NSString *CellIdentifier = @"Cell";
//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
//
//    // Configure the cell...
//
//    return cell;
//}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

/*
 #pragma mark - Navigation
 
 // In a story board-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 
 */

//Removing email and website contact options per app store guidelines for apps intended for kids under 13.
//-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    UITableViewCell *theCellClicked = [self.tableView cellForRowAtIndexPath:indexPath];
//    if (theCellClicked == webSiteCell) {
//        //Do stuff
//        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.muttstudios.com"]];
//    }
//    if (theCellClicked == emailCell) {
//        
//        NSLog(@"Mail Support.");
//        
//        // Email Subject
//        NSString *emailTitle = @"SwipeMath 1.1";
//        // Email Content
//        NSString *messageBody = @"";
//        // To address
//        NSArray *toRecipents = [NSArray arrayWithObject:@"support@muttstudios.com"];
//        
//        MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
//        mc.mailComposeDelegate = self;
//        [mc setSubject:emailTitle];
//        [mc setMessageBody:messageBody isHTML:NO];
//        [mc setToRecipients:toRecipents];
//        
//        // Present mail view controller on screen
//        [self presentViewController:mc animated:YES completion:NULL];
//        
//    }
//}
//- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
//{
//    switch (result)
//    {
//        case MFMailComposeResultCancelled:
//            NSLog(@"Mail cancelled");
//            break;
//        case MFMailComposeResultSaved:
//            NSLog(@"Mail saved");
//            break;
//        case MFMailComposeResultSent:
//            NSLog(@"Mail sent");
//            break;
//        case MFMailComposeResultFailed:
//            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
//            break;
//        default:
//            break;
//    }
//    // Close the Mail Interface
//    [self dismissViewControllerAnimated:YES completion:NULL];
//}


- (IBAction)valueChanged:(UIStepper *)sender {
    
        double value = [sender value];
        startNum = [sender value];
        AudioServicesPlaySystemSound(soundClick);
    
        [number1setting setText:[NSString stringWithFormat:@"%d", (int)value]];
    }

- (IBAction)valueChanged2:(UIStepper *)sender {
    
        double value = [sender value];
        endNum = [sender value];
        AudioServicesPlaySystemSound(soundClick);
    
        [number2setting setText:[NSString stringWithFormat:@"%d", (int)value]];
    }

- (IBAction)dismissModalView:(id)sender {
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"properties.plist"];
    
    NSMutableDictionary *myDict = [[NSMutableDictionary alloc] initWithContentsOfFile: path];
    
    [myDict setValue:[NSNumber numberWithInteger:startNum] forKey:@"start"];
    [myDict setValue:[NSNumber numberWithInteger:endNum] forKey:@"end"];
    
    [myDict writeToFile:path atomically:TRUE];
    

    [self dismissViewControllerAnimated:NO completion:nil];
}

@end
