//
//  aboutViewController.h
//  SwipeMath
//
//  Created by Skip Barker on 10/20/13.
//  Copyright (c) 2013 Mutt Studios. All rights reserved.
//
//  This file is part of SwipeMath.
//
//  SwipeMath is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  SwipeMath is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with SwipeMath.  If not, see <http://www.gnu.org/licenses/>.

#import <UIKit/UIKit.h>
#import <AudioToolbox/AudioToolbox.h>

@interface aboutViewController : UITableViewController {
    IBOutlet UIStepper *valueChanged;
    IBOutlet UIStepper *valueChanged2;
    UILabel *number1setting;
    UILabel *number2setting;
    SystemSoundID soundClick;
    SystemSoundID soundSwitch;
}


-(IBAction)dismissModalView:(id)sender;
-(IBAction)valueChanged:(id)sender;
-(IBAction)valueChanged2:(id)sender;

//Removing email and website contact options per app store guidelines for apps intended for kids under 13.
//@property (nonatomic, strong) IBOutlet UITableViewCell *webSiteCell;
//@property (nonatomic, strong) IBOutlet UITableViewCell *emailCell;

@property (nonatomic,retain) IBOutlet UIStepper *valueChanged;
@property (nonatomic,retain) IBOutlet UIStepper *valueChanged2;

@property (nonatomic,assign) int startNum;
@property (nonatomic,assign) int endNum;

@property (nonatomic,retain) IBOutlet UILabel *number1setting;
@property (nonatomic,retain) IBOutlet UILabel *number2setting;

@end
