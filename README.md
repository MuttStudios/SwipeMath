SwipeMath
=========
###SwipeMath is released under GPLv3.###

##### If you would like to support our work, you may purchase SwipeMath from the [AppStore](http://appstore.com/swipemath) for USD 99¢.

##### This is a simple flash card app in order to help kids study.

- Addition
- Subtraction
- Multiplication
- Division

##### The *direction* you swipe determines your math problem.   
- **Right** *Subtraction*. 
- **Left** *Addition*. 
- **Down** *Division*. 
- **Up** *Multiplication*.

##### Features include: #####
- Designed for iOS 7
- Set your own number range for questions. 
- Universal app. 
- Safe for kids. No in app purchases. No Social Media. No personal data collected. 
- Simple flash card style for easy math drills and test preparation. 
